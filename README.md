# calendar

Simple CLI app that prints the current class schedule, taken from the Univr/orari website.


### Installation
//TODO

### Usage
To get the current week schedule just run
```
cal 
``` 
This is the same as 
```
cal -week=current
```
To print a different week, just specify the date range (Monday-Sunday, format DD/MM-DD/MM), like so
```
cal -week=19/11-26/11
```

or just 
```
cal -week=19/11
```
